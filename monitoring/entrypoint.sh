#!/bin/sh
set -e

until psql -h ${DATABASE_URL:-db} -U ${DATABASE_USER:-postgres} -c '\l'; do
  echo "Postgres is unavailable - sleeping"
  sleep 1
done

echo "Postgres is up - continuing"

exec "$@"