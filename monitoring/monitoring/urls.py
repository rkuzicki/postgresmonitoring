from django.urls import path, include

urlpatterns = [
    path('', include('connection.urls')),
    path('stats/', include('stats.urls')),
]
