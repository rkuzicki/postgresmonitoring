const URL = '/stats/queries/';

function td(text, b = false) {
    return "<td>" + (b ? "<b>" : "") + text + (b ? "</b>" : "") + "</td>";
}

function setHeaders(host, name) {
    $('#db-name').html(name);
    $('#db-host').html(host)
}

function setSimpleData(data) {
    $('#amount-of-connections').html(data['amount_of_connections']);
    $('#connections-awaiting-lock').html(data['amount_of_connections_waiting_for_lock']);
    $('#db-size').html(data['database_size']);
    $('#max-transaction-age').html(data['maximum_transaction_age'])
}

function setConnectionsByState(connectionsByState) {
    let htmlString = "";
    for (const status of connectionsByState) {
        htmlString += "<p>Status: " + (status === null ? 'unknown' : status.status) + ", connections: " + status.count +
            "</p>"
    }
    $('#connections-by-state').html(htmlString);
}

function setRelationSizes(relationSizes, maxSize) {
    let htmlString = "";
    for (const relation of relationSizes) {
        htmlString += "<p><b>Name</b>: " + relation.name + ", <b>size</b> " + relation.size + '/' + maxSize + "</p>"
    }
    $('#relation-sizes').html(htmlString);
}

function setIndexes(indexes) {
    let htmlString = "";
    htmlString += "<tr>" + (td('Table', true) + td('Index name', true) + td('Row count', true)
        + td('Index Size', true)) + td('Unique', true) + "</tr>";
    for (const index of indexes) {
        htmlString += "<tr>" + td(index.table) + td(index.name) + td(index.row_count) +
            td(index.index_size) + td(index.unique) + "</tr>";
    }
    $('#indexes').html(htmlString);
}

function setLocks(locks) {
    let htmlString = "";
    htmlString += "<tr>" + (td('Blocked lock PID', true) + td('Blocking lock PID', true) + td('Blocked activity query', true)
        + td('Blocking activity query', true)) + td('Blocked activity user', true) + td('Blocking activity user', true) + "</tr>";
    for (const lock of locks) {
        htmlString += "<tr>" + td(lock.blocked_lock_pid) + td(lock.blocking_lock_pid) + td(lock['blocked_activity_query']) +
            td(lock['blocking_activity_query']) + td(lock['blocked_activity_user']) + td(lock['blocking_activity_user']) +  "</tr>";
    }
    $('#locks').html(htmlString);
}


function setData(data) {
    setHeaders(data.host, data.name);
    setSimpleData(data);
    setConnectionsByState(data['amount_of_connections_by_state']);
    setRelationSizes(data['relation_sizes'], data['database_size']);
    setIndexes(data['indexes']);
    setLocks(data['locks']);
}

function getQueryResults() {
    $.get(URL, function (data, status) {
        console.log(data);
        setData(data);
    })
}


setInterval(getQueryResults, 3000);
