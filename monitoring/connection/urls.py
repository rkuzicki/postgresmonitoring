from django.urls import path
from .views import MainRedirectView, ConnectionView

urlpatterns = [
    path('', MainRedirectView.as_view(), name='main_redirect_view'),
    path('connect/', ConnectionView.as_view(), name='connection_view'),
]
