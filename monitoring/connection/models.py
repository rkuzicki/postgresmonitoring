from django.db import models


class Database(models.Model):
    host = models.CharField(max_length=128)
    name = models.CharField(max_length=128)
    port = models.IntegerField()

    class Meta:
        unique_together = ('host', 'name', 'port')
