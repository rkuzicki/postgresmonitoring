from django import forms
from psycopg2 import connect, OperationalError


class DatabaseConnectionForm(forms.Form):
    host = forms.CharField()
    name = forms.CharField()
    port = forms.IntegerField()
    user = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)

    def is_valid(self):
        super().is_valid()
        try:
            connection = connect(
                user=self.cleaned_data['user'],
                password=self.cleaned_data['password'],
                host=self.cleaned_data['host'],
                port=self.cleaned_data['port'],
                database=self.cleaned_data['name']
            )
            cursor = connection.cursor()
            cursor.execute("SELECT 1")
            return True
        except OperationalError:
            return False



