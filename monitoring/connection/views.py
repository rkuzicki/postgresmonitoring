from django.views.generic.edit import View, FormView
from django.shortcuts import redirect
from django.urls import reverse_lazy

from .forms import DatabaseConnectionForm
from .models import Database

from logging import getLogger

logger = getLogger(__name__)


class MainRedirectView(View):
    def get(self, request, *args, **kwargs):
        if request.session.get('connected'):
            return redirect(reverse_lazy('stats_view'))
        else:
            return redirect(reverse_lazy('connection_view'))


class ConnectionView(FormView):
    template_name = 'connection/db_login.html'
    form_class = DatabaseConnectionForm
    success_url = reverse_lazy('stats_view')

    def _update_session(self, data):
        self.request.session['connected'] = True
        self.request.session['db'] = data
        self.request.session.modified = True

    def _get_or_save_db(self):
        session = self.request.session
        _, created = Database.objects.get_or_create(name=session['db'].get('name'),
                                                    host=session['db'].get('host'),
                                                    port=session['db'].get('port'))
        if created:
            logger.warning("Database saved")

    def form_valid(self, form):
        response = super().form_valid(form)
        self._update_session(form.cleaned_data)
        self._get_or_save_db()
        return response
