"""
Temporary solution after the proper database login is implemented
"""

DB_HOST = 'test_db'
DB_USER = 'postgres'
DB_PASSWORD = 'postgres'
DB_NAME = 'test_db'
DB_PORT = '5432'

LOCK_QUERY = """
SELECT blocked_locks.pid         AS blocked_pid, 
       blocked_activity.usename  AS blocked_user, 
       blocking_locks.pid        AS blocking_pid, 
       blocking_activity.usename AS blocking_user, 
       blocked_activity.query    AS blocked_statement, 
       blocking_activity.query   AS current_statement_in_blocking_process 
FROM   pg_catalog.pg_locks blocked_locks 
JOIN   pg_catalog.pg_stat_activity blocked_activity 
ON     blocked_activity.pid = blocked_locks.pid 
JOIN   pg_catalog.pg_locks blocking_locks 
ON     blocking_locks.locktype = blocked_locks.locktype 
AND    blocking_locks.database IS NOT distinct 
FROM   blocked_locks.DATABASE 
AND    blocking_locks.relation IS NOT DISTINCT 
FROM   blocked_locks.relation 
AND    blocking_locks.page IS NOT DISTINCT 
FROM   blocked_locks.page 
AND    blocking_locks.tuple IS NOT DISTINCT 
FROM   blocked_locks.tuple 
AND    blocking_locks.virtualxid IS NOT DISTINCT 
FROM   blocked_locks.virtualxid 
AND    blocking_locks.transactionid IS NOT DISTINCT 
FROM   blocked_locks.transactionid 
AND    blocking_locks.classid IS NOT DISTINCT 
FROM   blocked_locks.classid 
AND    blocking_locks.objid IS NOT DISTINCT 
FROM   blocked_locks.objid 
AND    blocking_locks.objsubid IS NOT DISTINCT 
FROM   blocked_locks.objsubid 
AND    blocking_locks.pid != blocked_locks.pid 
JOIN   pg_catalog.pg_stat_activity blocking_activity 
ON     blocking_activity.pid = blocking_locks.pid 
WHERE  NOT blocked_locks.granted; 
"""

QUERIES = {
    'amount_of_connections': "SELECT COUNT(*) FROM pg_stat_activity;",
    'amount_of_connections_by_state': "SELECT state, COUNT(*) FROM pg_stat_activity GROUP BY state;",
    'amount_of_connections_waiting_for_lock': "SELECT COUNT(DISTINCT pid) FROM pg_locks WHERE granted = false;",
    'maximum_transaction_age': "SELECT MAX(NOW() - xact_start) FROM pg_stat_activity WHERE state IN ('idle in transaction', 'active');",
    # 'running_queries': "SELECT S.pid, age(clock_timestamp(), query_start), usename, query, L.mode, L.locktype, L.granted FROM pg_stat_activity S inner join pg_locks L on S.pid = L.pid order by L.granted, L.pid DESC",
    'database_size': 'SELECT pg_size_pretty(pg_database_size(current_database()));',
    'relation_sizes': "SELECT nspname || '.' || relname AS \"relation\", pg_size_pretty(pg_total_relation_size(C.oid)) AS \"total_size\" FROM pg_class C LEFT JOIN pg_namespace N ON (N.oid = C.relnamespace)  WHERE nspname NOT IN ('pg_catalog', 'information_schema') AND C.relkind <> 'i' AND nspname !~ '^pg_toast' ORDER BY pg_total_relation_size(C.oid) DESC;",
    'indexes': "SELECT t.tablename, indexname, c.reltuples AS num_rows, " \
               "pg_size_pretty(pg_relation_size(quote_ident(indexrelname)::text)) AS index_size, " \
               "CASE WHEN indisunique THEN 'Y' ELSE 'N' END AS UNIQUE, idx_scan AS number_of_scans, idx_tup_read AS tuples_read, " \
               "idx_tup_fetch AS tuples_fetched FROM pg_tables t " \
               "LEFT OUTER JOIN pg_class c ON t.tablename=c.relname " \
               "LEFT OUTER JOIN ( SELECT c.relname AS ctablename, ipg.relname AS indexname, x.indnatts AS number_of_columns, idx_scan, idx_tup_read, idx_tup_fetch, indexrelname, indisunique FROM pg_index x JOIN pg_class c ON c.oid = x.indrelid JOIN pg_class ipg ON ipg.oid = x.indexrelid JOIN pg_stat_all_indexes psai ON x.indexrelid = psai.indexrelid )" \
               " AS foo ON t.tablename = foo.ctablename WHERE t.schemaname='public' ORDER BY 1,2;",
    'locks': LOCK_QUERY,
}

