from django.urls import path
from .views import StatsView, QueryAPIView

urlpatterns = [
    path('', StatsView.as_view(), name='stats_view'),
    path('queries/', QueryAPIView.as_view(), name='query_executor')
]
