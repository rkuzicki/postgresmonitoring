from .serializers import *

QUERY_SERIALIZER_DATA = {
    'amount_of_connections': CountResultSerializer,
    'amount_of_connections_by_state': ConnectionsByStateSerializer,
    'amount_of_connections_waiting_for_lock': CountResultSerializer,
    'maximum_transaction_age': TimeSingleSerializer,
    'running_queries': LongQuerySerializer,
    'database_size': DatabaseSizeSerializer,
    'relation_sizes': RelationSizeSerializer,
    'indexes': IndexDataSerializer,
    'locks': LockQuerySerializer,
}


def get_serialized_data(query, data):
    serializer_class = QUERY_SERIALIZER_DATA.get(query)
    if not serializer_class:
        raise NotImplementedError("No serializer for given query")
    return serializer_class(data).serialize()
