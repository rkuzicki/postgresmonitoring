from connection.models import Database
from django.db import models


class Lock(models.Model):
    blocked_lock_pid = models.IntegerField()
    blocking_lock_pid = models.IntegerField()
    blocked_activity_query = models.CharField(max_length=4096)
    blocking_activity_query = models.CharField(max_length=4096)
    blocked_activity_user = models.CharField(max_length=1024)
    blocking_activity_user = models.CharField(max_length=1024)
    date = models.DateTimeField(auto_now_add=True)
    database = models.ForeignKey(Database, on_delete=models.CASCADE)


class Size(models.Model):
    date = models.CharField(primary_key=True, max_length=128)
    size = models.CharField(max_length=128)
    database = models.ForeignKey(Database, on_delete=models.CASCADE)
