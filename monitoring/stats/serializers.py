from abc import ABC, abstractmethod
from rest_framework import serializers
from .models import Lock


class QueryResultSerializer(ABC):

    def __init__(self, data):
        self.data = data

    @abstractmethod
    def serialize(self):
        raise NotImplementedError("Implement this method")


class CountResultSerializer(QueryResultSerializer):
    def serialize(self,):
        return int(self.data[0][0])


class ConnectionsByStateSerializer(QueryResultSerializer):
    def serialize(self):
        res = []
        for row in self.data:
            res.append({
                'status': row[0],
                'count': row[1],
            })
        return res


class TimeSingleSerializer(QueryResultSerializer):
    def serialize(self):
        return self.data[0][0].seconds


class LongQuerySerializer(QueryResultSerializer):
    def serialize(self):
        res = []
        for row in self.data:
            res.append({
                'user': row[2],
                'query': row[3],
                'time': row[1].seconds
            })
        return res


class DatabaseSizeSerializer(QueryResultSerializer):
    def serialize(self):
        return self.data[0][0]


class RelationSizeSerializer(QueryResultSerializer):
    def serialize(self):
        res = []
        for row in self.data:
            res.append({
                'name': row[0],
                'size': row[1]
            })
        return res


class IndexDataSerializer(QueryResultSerializer):
    def serialize(self):
        res = []
        for row in self.data:
            res.append({
                'table': row[0],
                'name': row[1],
                'row_count': row[2],
                'index_size': row[3],
                'unique': True if row[4] == 'Y' else False
            })
        return res


class LockQuerySerializer(QueryResultSerializer):
    def serialize(self):
        res = []
        for row in self.data:
            res.append({
                'blocked_locks_pid': row[0],
                'blocked_activity_user': row[1],
                'blocking_locks_pid': row[2],
                'blocking_activity_user': row[3],
                'blocked_activity_query': row[4],
                'blocking_activity_query': row[5],
            })
        return res


class LockQuerysetSerializers(serializers.ModelSerializer):
    class Meta:
        model = Lock
        fields = ['blocked_lock_pid', 'blocking_lock_pid', 'blocked_activity_query',
                  'blocking_activity_query', 'blocked_activity_user', 'blocking_activity_user']

