from datetime import date

from django.views.generic import TemplateView
from django.http.response import JsonResponse
from django.shortcuts import redirect
from django.urls import reverse_lazy

from rest_framework.views import APIView
import psycopg2

from connection.models import Database

from .constants import *
from .models import Lock, Size
from .utils import get_serialized_data
from .serializers import LockQuerysetSerializers

from logging import getLogger
logger = getLogger(__name__)


class StatsView(TemplateView):
    template_name = 'stats/stats.html'

    def get(self, request, *args, **kwargs):
        if not request.session.get('connected'):
            return redirect(reverse_lazy('connection_view'))
        return super().get(request, *args, **kwargs)


class QueryAPIView(APIView):

    def _prepare_data(self, cursor):
        data = {}
        for name, query in QUERIES.items():
            cursor.execute(query)
            res = cursor.fetchall()
            data[name] = get_serialized_data(name, res)
            data['host'] = DB_HOST
            data['name'] = DB_NAME
        return data

    def _get_db(self, session):
        try:
            return Database.objects.get(host=session['db'].get('host'),
                                        port=session['db'].get('port'),
                                        name=session['db'].get('name'))
        except Database.DoesNotExist:
            return None

    def _save_locks(self, locks, db):
        for lock in locks:
            db_lock, created = Lock.objects.get_or_create(
                blocked_lock_pid=lock['blocked_locks_pid'],
                blocking_lock_pid=lock['blocking_locks_pid'],
                database=db

            )
            if created:
                logger.warning("Lock created")
                db_lock.blocked_activity_query = lock['blocked_activity_query']
                db_lock.blocking_activity_query = lock['blocking_activity_query']
                db_lock.blocked_activity_user = lock['blocked_activity_user']
                db_lock.blocking_activity_user = lock['blocking_activity_user']
                db_lock.save()
            else:
                logger.warning("Lock already in database")

    def _save_size(self, size, db):
        today = date.today().strftime("%Y-%m-%d")
        Size.objects.update_or_create(date=today, database=db, size=size)

    def get(self, request):
        db = self._get_db(request.session)
        if not request.session.get('connected'):
            return JsonResponse({'error': 'You are not connected to the database'})
        session = request.session
        try:
            connection = psycopg2.connect(user=session['db'].get('user'),
                                          password=session['db'].get('password'),
                                          host=session['db'].get('host'),
                                          port=session['db'].get('port'),
                                          database=session['db'].get('name'))
            cursor = connection.cursor()
            data = self._prepare_data(cursor)
            self._save_locks(data['locks'], db)
            self._save_size(data['database_size'], db)
            locks = Lock.objects.filter(database=db)
            data['locks'] = LockQuerysetSerializers(locks, many=True).data
        except psycopg2.Error as e:
            print(e)
        else:
            connection.close()
            return JsonResponse(data)

